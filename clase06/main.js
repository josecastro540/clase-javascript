$(document).ready(function(){

	var taskStorage = JSON.parse(localStorage.getItem("tasks"));

	// if (taskStorage == null) {
	// 	var tasks = [];
	// }else{
	// 	var tasks = taskStorage;
	// }

	taskStorage == null ? tasks = [] : tasks = taskStorage;

	// FUNCION IMPRIMIR TODAS LAS TAREAS
	printAll();

	// FUNCION SALVAR LAS TAREAS
	function guardar(){
		var input = $("#task").val();

		if (input == "" || input == null) {
			$(".alert").text("Por favor escribe tu tarea");
		}else{
			tasks.push({'tarea': input, 'hecho': false});
			localStorage.setItem("tasks", JSON.stringify(tasks));
			$('.alert').text("");

			// FUNCION IMPRIMIR EL ELEMENTO SALVADO;
			printUno();
		}
		$('#task').val("");
	}

	//FUNCION PARA IMPRIMIR TODAS LAS TAREAS
	function printAll(){
		for(i in taskStorage){

			if(tasks[i].hecho === true){
				$('#task-list').append(`
					<li class="checked" data-value="${tasks[i].tarea}">
						${taskStorage[i].tarea}
						<span class="close" data-class="${tasks[i].tarea}">
							<i class="fa fa-times"></i>
						</span>
					</li>
				`);
			}else{
				$('#task-list').append(`
					<li data-value="${tasks[i].tarea}">
						${taskStorage[i].tarea}
						<span class="close" data-class="${tasks[i].tarea}">
							<i class="fa fa-times"></i>
						</span>
					</li>
				`);
			} 
		}
	}

	// IMPRIMIR UN ELEMENTO

	function printUno(){
		for(i in tasks){
			var template = `
				<li data-value="${tasks[i].tarea}">
						${taskStorage[i].tarea}
						<span class="close" data-class="${tasks[i].tarea}">
							<i class="fa fa-times"></i>
						</span>
					</li>`;
		}

		$('#task-list').append(template);
	}


	// BORRAR EL ELEMENTO
	$(document).on("click",".close", function(e){
		//Evitar que el padre reciba el click
		e.stopPropagation();
		$(this).parent().fadeOut();

		//Buscar en el array y eliminar el json del array

		function findAndRemove(array, propiedad, valor){
			array.forEach(function(result, index){
				if(result[propiedad] === valor){
					// Elimina el array
					array.splice(index,1);
				}	
			});
		}

		findAndRemove(tasks,'tarea', $(this).attr('data-class'));

		localStorage.setItem('tasks', JSON.stringify(tasks));

	})

	// MARCAR LA TAREA COMO REALIZADA
	$(document).on('click', "#task-list li", function(){
		// Buscar en el array y marchar como hecho el json del mismo
		function findAndReplace(array, propiedad, value){
			array.forEach(function(result, index){
				if (result[propiedad] === value && array[index].hecho === false){
					//Remplazamos en el array
					array[index].hecho = true;
				}else if(result[propiedad] === value && array[index].hecho === true){
					array[index].hecho = false;
				}
			})
		}

		findAndReplace(tasks, 'tarea', $(this).attr('data-value'));
		//Lo persisto en localStorage
		localStorage.setItem("tasks", JSON.stringify(tasks));
		//console.log(tasks);
		$(this).toggleClass('checked');


	});



	// EJECUCION DE FUNCIONES
	$('#save').click(function(){
		guardar();
	})


}); //Fin del document ready