// var celular = {
// 	pantalla: 'samsung',
// 	bateria: 12,
// 	camara_frontal: '5mpx',
// 	camara_trasera: '12mpx',
// 	llamar: function(){
// 		return 'Estoy llamando';
// 	},
// 	foto: function(){
// 		console.log('Saliste bien');
// 	}
// };

// var template = `la pantalla es ${celular.pantalla}, su bateria dura ${celular.bateria} horas,
// 	${celular.llamar()}
// 	`;

// console.log(template);

var celular = {
	iphone: {
		modelos: [4,'4s',5,'5s',6]
	},
	samsung: {
		modelos: ['Galaxy', 'GalaxyS4', 'GalaxyS5', 'GalaxyS6']
	},
	motorola: {
		modelos: ['G1', 'G2', 'G3', 'Z3']
	}
}

celular.motorola.modelos.push('E');

var cel = $('#cel');
console.log(cel);

for(i in celular.samsung.modelos){
	cel.append('<li>'+celular.samsung.modelos[i]+'</li>');
}
 // Crear objetos con funciones constructoras


 function Persona(nombre, sexo, edad, nacionalidad){
 	this.nombre = nombre;
 	this.sexo = sexo;
 	this.edad = edad;
 	this.nacionalidad = nacionalidad;
 	this.saludar = function(){
 		return "Hola mi nombre es "+this.nombre;
 	};
 };

 var andre = new Persona("Andre Vielma", "Masculino", 24, "Venezolano");

 var jose = new Persona("José Castro", "Masculino", 31, "Venezolano");

andre.programa = "PHP";

Persona.prototype.programa = null;
Persona.prototype.caminar = function(){
	return "Estoy caminando";
}


 console.log(andre.programa);
 console.log(jose.caminar());


