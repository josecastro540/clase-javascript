function palindromo(cadena){
	// convertir en minuscula
	var cadenaOri = cadena.toLowerCase();
	// convertir en un array
	var cadenaArray = cadenaOri.split("");
	//quitar espacion en blanco

	var stringWhite = "";//string

	for( i in cadenaArray){
		if(cadenaArray[i] != " "){
			stringWhite += cadenaArray[i]
		}
	}

	// Vuelvo a convertir en array la cadena si espacios y creo otra con reverse

	var letter = stringWhite.split("");
	var letterReverse = stringWhite.split("").reverse();

	// compara las dos variables
	var iguales = true;

	for(i in letter){
		if(letter[i] == letterReverse[i]){
			// no cambia cambia nada
		}else {
			iguales = false;
		}
	}	

	if (iguales) {
		return `El string <strong style= "font-family: arial; color: #34495e;"> ${cadena} </strong> es un palindromo`;
	}else{
		return `El string <strong style= "font-family: arial; color: #e74c3c;"> ${cadena} </strong> NO es un palindromo`;
	}

}

var frase = prompt("Escribe una palabra o frase para saber si es un palindromo")

document.write(palindromo(frase));