$(document).ready(function(){
	var numero = [];
	var resultado = [];
	$('#valores').text(0);

	$('.btn').click(function(){
		if($(this).val() != "=" && $(this).val()!= 'ce' && $(this).val()!= "c"){
			numero.push($(this).val());
			$('#valores').html(numero);
			// console.log(numero);
			// console.log(igual());
		}	
	});

	$('#igual').click(function(){
		igual();
	});

	$('#clear').click(function(){
		borrar();
	})

	$('#ce').click(function(){
		borrarLast();
	})


	// Eventos del Teclado
	$(document).keypress(function(event){
		// var key = event.keyCode;
		var key = event.which;
		console.log(key);

		switch(key){
			case 48:
			valor(0);
			addColor($('#cero'),'keypress');
			break;

			case 49:
			valor(1);
			addColor($('#uno'),'keypress');
			break;

			case 50:
			valor(2);
			addColor($('#dos'),'keypress');
			break;

			case 51:
			valor(3);
			addColor($('#tres'),'keypress');
			break;

			case 52:
			valor(4);
			addColor($('#cuatro'),'keypress');
			break;

			case 53:
			valor(5);
			addColor($('#cinco'),'keypress');
			break;

			case 54:
			valor(6);
			addColor($('#seis'),'keypress');
			break;

			case 55:
			valor(7);
			addColor($('#siete'),'keypress');
			break;

			case 56:
			valor(8);
			addColor($('#ocho'),'keypress');
			break;

			case 57:
			valor(9);
			addColor($('#nueve'),'keypress');
			break;

			case 46:
			valor('.');
			addColor($('#punto'),'keypress');
			break;

			// OPERADORES
			case 43:
			valor('+');
			addColor($('#mas'),'operadores');
			break;

			case 45:
			valor('-');
			addColor($('#menos'),'operadores');
			break;

			case 13:
			igual();
			addColor($('#igual'),'operadores');
			break;

			case 42:
			valor('*');
			addColor($('#por'),'operadores');
			break;

			case 47:
			valor('/');
			addColor($('#entre'),'operadores');
			break;

			// clear
			case 99:
			borrar();
			addColor($('#clear'),'borrar');
			break;

		}
	});

	$(document).keydown(function(event){
		if(event.which == 27){
			addColor($('#clear'),'borrar');
			borrar();
		}

		if(event.which == 8){
			addColor($('#ce'),'borrar');
			borrarLast();
		}

	});


	$(document).keyup(function(event){
		$('.btn').removeClass('keypress');
		$('.btn').removeClass('operadores');
		$('.btn').removeClass('borrar');
	});

	// FUNCIONES PROPIAS

	function igual(){
		var numeros = numero.join("");
		 try{
		 	if(isNaN(eval(numeros))){
		 		$('#valores').html("ERROR");
		 	}else{
		 		resultado.push(eval(numeros));
		 		numero = resultado;
		 		resultado = [];
		 		$('#valores').html(eval(numeros));
		 	}
		 }
		 catch(err){
		 	console.log(err.name);
		 	$('#valores').html("ERROR");
		 }

		// return eval(numeros);
	}

	function borrar(){
		$("#valores").html("0");
		numero = [];
	}

	function borrarLast(){
		numero.pop();

		numeros = numero.join("");
		if (numero.length == 0) {
			$("#valores").html("0");
		}else{
			$("#valores").html(numeros);
		}

	}

	function valor(val){
		numero.push(val);
		$('#valores').html(numero);
	}


	function addColor(btn,clase){
		btn.addClass(clase);
	}
});//End document ready
