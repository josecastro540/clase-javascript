'use stric'

const http = require('http');
const port = 3000;

var server = http.createServer((request, response)=>{
	response.writeHead(200,{"Content-Type":"text/html"});
	response.write('Hola Mundo desde node');
	response.end();
});

server.listen(port);

console.log("Servidor corriendo en el puerto "+port);